<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{

    public function create(){
        return view('casts.create');
    }
    
    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'nama'=>'required|unique:cast',
            'bio'=>'required|max:255',
            'umur'=>'nullable'
        ]);
        // $query = DB::table('cast')->insert([
        //     "nama"=> $request["nama"],
        //     "bio"=> $request["bio"],
        //     "umur"=> $request["umur"]
        // ]);
        // $cast = Cast::create([
        //     "nama"=> $request["nama"],
        //     "bio"=> $request["bio"],
        //     "umur"=> $request["umur"]
        // ]);
        $cast = new Cast;
        $cast->nama = $request["nama"];
        $cast->bio = $request["bio"];
        $cast->umur = $request["umur"];
        $cast->save();
        return redirect('/cast')->with('success', 'Cast has been added!');
    }

    public function index(){
        $cast = Cast::all();
        // $cast = DB::table('cast')->get();
        // dd($temp);
        return view('casts.index', compact('cast'));
    }

    public function show($id){
        $cast = Cast::find($id);
        // $cast = DB::table('cast')->where('id', $id)->first();
        // dd($cast);
        return view('casts.show', compact('cast'));
    }

    public function edit($id){
        $cast = Cast::find($id);
        // $cast = DB::table('cast')->where('id', $id)->first();
        
        return view('casts.edit',compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama'=>'required|unique:cast',
            'bio'=>'required|max:255',
            'umur'=>'nullable'
        ]);
        $query = Cast::where('id', $id)->update([
            "nama"=>$request["nama"],
            "bio"=>$request["bio"],
            "umur"=>$request["umur"]
        ]);
        // $query = DB::table('cast')->where('id',$id)
        // ->update([
        //     'nama'=>$request['nama'],
        //     'bio'=>$request['bio'],
        //     'umur'=>$request['umur']
        // ]);
        return redirect('/cast')->with('success', 'Data has been updated');
    }

    public function destroy($id){
        Cast::destroy($id);
        // $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Cast has been deleted');
    }
}
