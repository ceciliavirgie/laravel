<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('register');
    }
    public function welcome(Request $request){
        $nama = $request["nama"];
        $nama2 = $request["nama2"];
        return view('welcome', compact('nama','nama2'));
    }
}
