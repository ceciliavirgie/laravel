<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
    <label>First Name :</label> <br><br>
    <input type="text" name = "nama"> <br><br>
    <label>Last Name :</label> <br><br>
    <input type="text" name="nama2"> <br><br>
    <label>Gender :</label> <br><br>
    <input type="radio" name ="gender"> Male <br>
    <input type="radio" name ="gender"> Female <br>
    <input type="radio" name ="gender"> Other <br><br>
    <label>Nationality :</label> <br><br>
    <select name="national" id="">
        <option value="Indonesian">Indonesian</option>
        <option value="Singapore">Singapore</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Australian">Australian</option>
    </select><br><br>
    <label >Language Spoken :</label><br><br>
    <input type="checkbox" name ="language" value = "Bahasa Indonesia"> Bahasa Indonesia <br>
    <input type="checkbox" name ="language" value = "English"> English <br>
    <input type="checkbox" name="language" value = "Other"> Other <br><br>
    <label>Bio :</label><br><br>
    <textarea name="bio" id="" cols="30" rows="10">
    </textarea><br><br>
    <input type="submit" value = "Sign up">
    </form>
</body>
</html>