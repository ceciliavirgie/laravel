@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Cast Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success')}}
                    </div>
                @endif
                <a href="{{ route('cast.create')}}" class ="btn btn-primary mb-2">Input New Cast</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Name</th>
                      <th>Bio</th>
                      <th>Age</th>
                      <th style="width: 40px">Tambah</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($cast as $key => $casts)
                    <tr>
                        <td>{{ $key +1 }}</td>
                        <td>{{ $casts->nama}}</td>
                        <td>{{ $casts->bio}}</td>
                        <td>{{ $casts->umur}}</td>
                        <td style="display: flex;">
                            <a href="/cast/{{$casts->id}}" class="btn btn-info btn-sm mr-2">Show</a>
                            <a href="/cast/{{$casts->id}}/edit" class="btn btn-default btn-sm mr-2">Edit</a>
                            <form action="/cast/{{$casts->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                    @empty
                        <td colspan="5" align="center">No Data</td>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
</div>
@endsection