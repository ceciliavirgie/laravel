@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
    <h1>{{$cast->nama}}</h1>   
    <p>Biodata : {{$cast->bio}}</p>
    <p>Umur : {{$cast->umur}}</p>
</div>
@endsection