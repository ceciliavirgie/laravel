@extends('adminlte.master');

@section('content')
<div class = "ml-3 mt-3">
  <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form New Cast</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role = "form" action="{{route('cast.store')}}" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Name</label>
                    <input type="text" class="form-control" id="name" name = "nama" value="{{old('nama', '')}}" placeholder="Enter Name">
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name = "bio" value="{{old('bio', '')}}" placeholder="Enter Bio">
                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-age">
                    <label for="umur">Age</label>
                    <input type="Integer" class="form-control" id="umur" name = "umur" value="{{old('umur', '')}}" placeholder="Enter Age">
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
</div>
@endsection