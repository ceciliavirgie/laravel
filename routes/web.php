<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/home','HomeController@home' ,function () {
//     return view('Home');
// });

// Route::get('/register','AuthController@form' ,function(){
//     return view('register');
// });

// Route::get('/welcome','AuthController@welcome', function(){
//     return view('welcome');
// });

Route::get('/home','HomeController@home');

Route::get('/register','AuthController@form');

Route::post('/welcome','AuthController@welcome');

Route::get('/master', function(){
    return view('adminlte.master');
});

Route::get('/table', function(){
    return view('RouteTable.table');
});

Route::get('/data-table',function(){
    return view('RouteTable.data-table');
});

// Route::get('/cast/create', 'CastController@create');

// Route::post('/store','CastController@store');

// Route::get('/cast', 'CastController@index');

// Route::get('/cast/{cast_id}', 'CastController@show');

// Route::get('/cast/{cast_id}/edit','CastController@edit');

// Route::put('/cast/{cast_id}', 'CastController@update');

// Route::delete('/cast/{cast_id}','CastController@destroy');

Route::resource('cast', 'CastController');